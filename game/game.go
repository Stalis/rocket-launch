package game

import (
	"gogame/game/shapes"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	ScreenWidth  = 800
	ScreenHeight = 600
)

type MyGame struct {
	BaseColor color.Color
	Position  shapes.Coord
	Shapes    []shapes.Shape

	sceneManager *SceneManager
	input        Input
}

func NewGame() *MyGame {
	return &MyGame{
		BaseColor: color.White,
		Shapes: []shapes.Shape{
			&shapes.Rect{
				Coord: shapes.Coord{
					X: 0,
					Y: 0,
				},
				Width:  20,
				Height: 20,
			},
		},
		input: Input{},
	}
}

func (g *MyGame) Update() error {
	if g.sceneManager == nil {
		g.sceneManager = &SceneManager{}
		g.sceneManager.GoTo(&MainMenuScene{BaseColor: g.BaseColor})
	}

	g.input.Update()
	if err := g.sceneManager.Update(&g.input); err != nil {
		return err
	}

	// delta := shapes.Coord{}

	// switch true {
	// case ebiten.IsKeyPressed(ebiten.KeyArrowUp):
	// 	delta.Y -= 1
	// case ebiten.IsKeyPressed(ebiten.KeyArrowLeft):
	// 	delta.X -= 1
	// case ebiten.IsKeyPressed(ebiten.KeyArrowDown):
	// 	delta.Y += 1
	// case ebiten.IsKeyPressed(ebiten.KeyArrowRight):
	// 	delta.X += 1
	// }

	// g.Position = g.Position.Add(delta)

	return nil
}

func (g *MyGame) Draw(screen *ebiten.Image) {
	g.sceneManager.Draw(screen)
}

func (g *MyGame) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return ScreenWidth, ScreenHeight
}
