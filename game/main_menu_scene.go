package game

import (
	"gogame/game/shapes"
	"image/color"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/pkg/errors"
)

type Button struct {
	shapes.Rect
	Action func() error
}

func (b *Button) IsIntersect(c shapes.Coord) bool {
	return b.IsIntersectXY(c.X, c.Y)
}

func (b *Button) IsIntersectXY(x, y int) bool {
	return x >= b.X && x <= b.X+b.Width &&
		y >= b.Y && y <= b.Y+b.Height
}

type MainMenuScene struct {
	BaseColor color.Color
	buttons   []*Button
}

func (m *MainMenuScene) Init() {
	m.buttons = []*Button{
		{
			Rect: shapes.Rect{
				Coord: shapes.Coord{
					X: 10,
					Y: 10,
				},
				Width:  400,
				Height: 100,
			},
			Action: func() error {
				log.Println("New Game")
				return nil
			},
		},
		{
			Rect: shapes.Rect{
				Coord: shapes.Coord{
					X: 10,
					Y: 120,
				},
				Width:  400,
				Height: 100,
			},
			Action: func() error {
				log.Println("Quit")
				return errors.New("Quit game")
			},
		},
	}
}

func (m *MainMenuScene) Update(gs *GameState) error {
	x, y := gs.Input.ClickPosition()

	if x >= 0 && y >= 0 {
		for _, btn := range m.buttons {
			if btn.IsIntersectXY(x, y) {
				if err := btn.Action(); err != nil {
					return err
				}
				break
			}
		}
	}

	return nil
}

func (m *MainMenuScene) Draw(screen *ebiten.Image) {
	for _, shape := range m.buttons {
		for _, pixel := range shape.Pixels() {
			screen.Set(pixel.X, pixel.Y, m.BaseColor)
		}
	}

	line := shapes.Line{
		Start: shapes.Coord{
			X: 50,
			Y: 50,
		},
		End: shapes.Coord{
			X: 50,
			Y: 50,
		},
	}
	for _, pixel := range line.Pixels() {
		screen.Set(pixel.X, pixel.Y, m.BaseColor)
	}
}
