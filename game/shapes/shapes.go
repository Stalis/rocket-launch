package shapes

import (
	"gogame/game/utils"
)

type Shape interface {
	Pixels() []Coord
}

type Line struct {
	Start Coord
	End   Coord
}

func (l *Line) Pixels() []Coord {
	res := []Coord{}

	// Bresenham's line algorithm
	dx := utils.AbsInt(l.End.X - l.Start.X)
	sx := utils.SignInt(l.End.X - l.Start.X)
	dy := -utils.AbsInt(l.End.Y - l.Start.Y)
	sy := utils.SignInt(l.End.Y - l.Start.Y)
	err := dx + dy

	x := l.Start.X
	y := l.Start.Y

	for {
		res = append(res, Coord{x, y})
		if x == l.End.X && y == l.End.Y {
			break
		}

		e2 := 2 * err

		if e2 >= dy {
			err += dy
			x += sx
		}
		if e2 <= dx {
			err += dx
			y += sy
		}
	}

	return res
}

type FilledRect struct {
	Coord
	Width, Height int
}

func (r *FilledRect) Pixels() []Coord {
	res := make([]Coord, r.Width*r.Height)
	i := 0

	for x := r.X; x < r.X+r.Width; x++ {
		for y := r.Y; y < r.Y+r.Height; y++ {
			res[i] = Coord{x, y}
			i++
		}
	}

	return res
}

type Rect struct {
	Coord
	Width, Height int
}

func (r *Rect) Pixels() []Coord {
	res := make([]Coord, r.Width*2+r.Height*2)
	i := 0

	for x := r.X; x <= r.X+r.Width; x++ {
		res[i] = Coord{x, r.Y}
		i++
		res[i] = Coord{x, r.Y + r.Height}
		i++
	}

	for y := r.Y + 1; y < r.Y+r.Height; y++ {
		res[i] = Coord{r.X, y}
		i++
		res[i] = Coord{r.X + r.Width, y}
		i++
	}

	return res
}

type Multishape []Shape

func (m *Multishape) Pixels() []Coord {
	res := make([]Coord, 10)
	for _, shape := range *m {
		res = append(res, shape.Pixels()...)
	}

	return res
}
