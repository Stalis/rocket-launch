package shapes

import "math"

type Coord struct {
	X, Y int
}

func (c *Coord) Add(other Coord) Coord {
	return c.AddXY(other.X, other.Y)
}

func (c *Coord) AddXY(x, y int) Coord {
	return Coord{c.X + x, c.Y + y}
}

func (c *Coord) DistanceTo(other Coord) float64 {
	return c.DistanceToXY(other.X, other.Y)
}

func (c *Coord) DistanceToXY(x, y int) float64 {
	x2 := math.Pow(float64(x-c.X), 2)
	y2 := math.Pow(float64(y-c.Y), 2)
	return math.Sqrt(x2 + y2)
}
