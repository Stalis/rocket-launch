//go:build ignore

package utils

import "github.com/cheekybits/genny/generic"

//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Number=NUMBERS"

type Number generic.Type

func AbsNumber(x Number) Number {
	if x < 0 {
		return -x
	}

	return x
}

func SignNumber(x Number) Number {
	return x / AbsNumber(x)
}
