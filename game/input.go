package game

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type Input struct {
	mouseX, mouseY int
	lmbPressed     bool
}

func (i *Input) Update() {
	i.lmbPressed = ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft)
	i.mouseX, i.mouseY = ebiten.CursorPosition()
}

func (i *Input) ClickPosition() (x, y int) {
	if i.lmbPressed {
		return i.mouseX, i.mouseY
	}

	return -1, -1
}
